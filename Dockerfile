FROM centos:7
MAINTAINER David Reagan <david@reagannetworks.com>
RUN mkdir -p /data/{duplicati,backup}
COPY installers/duplicati.rpm /opt/duplicati.rpm
RUN yum install -y epel-release
RUN rpm --import "https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF"
COPY files/etc_yum.repos.d_mono-centos7-stable.repo /etc/yum.repos.d/mono-centos7-stable.repo
RUN yum -y distribution-synchronization
RUN yum install -y /opt/duplicati.rpm
