# Duplicati-In-Docker
This is a project that makes it simple to use Duplicati via a Docker image.

**Images Current Version**: Duplicati-2.0.4.5

# Build
* Download the latest Duplicati RPM from [https://www.duplicati.com/download](https://www.duplicati.com/download) to the installers directory.
* Rename the file to duplicati.rpm.
* Run `docker build --network host -t registry.gitlab.com/jerrac/duplicati-in-docker:prod .`

# Use

## docker-cli

### Example backup job
Modify as needed. You can remove the docker cpu-quota and memory limits.

You might want to find a way to prevent the backup passphrase and any authentication tokens from being saved to your shell's command history.

The basic idea that you bind the `/duplicati_storage` (or other location to store persistent data) to `/data/duplicati`. Then you bind the directories you want to backup to `/data/backup`. From there you just configure the duplicati-cli options you wish to use.

> Note: The `--dblock-size`, `--keep-time`, etc. flags are just examples. Please make sure you know what they do before you use them. :)

#### Single backup job
```bash
$ docker run \
   --name duplicati-in-docker \
   --network host \
   --cpu-quota 50000 \
   -m 500M \
   --memory-reservation 400M \
   --volume "$PWD"/duplicati_storage:/data/duplicati \
   --volume /path/to/directory/that/you/want/to/backup:/data/backup/dirname:ro \
   --volume /path/to/directory/that/you/want/to/backup2:/data/backup/dirname2:ro \
   registry.gitlab.com/jerrac/duplicati-in-docker/duplicati:prod \ # Change this to your own image if you want to build it yourself.
   duplicati-cli backup \
     <backup destination> \
     /data/backup/ \
     --backup-name="<backup name>" \
     --dbpath=/data/duplicati/<backup name>.sqlite \
     --encryption-module=aes \
     --compression-module=zip \
     --dblock-size=500MB \
     --keep-time=7Y \
     --passphrase="<backup passphrase>" \
     < any other duplicati-cli options > \
     --disable-module=console-password-input
```

### Run Repair
```bash
docker run \
   --name duplicati-in-docker \
   --cpu-quota 50000 \
   -m 500M \
   --memory-reservation 400M \
   --volume "$PWD"/duplicati_storage:/data/duplicati \
   --volume /tmp/didtarget:/data/target:rw \
   --volume /home/reagand/tmp:/data/backup/reagand-tmp:ro \
   registry.gitlab.com/jerrac/duplicati-in-docker:prod \
   duplicati-cli repair \
     <backup destination>  \
     --backup-name="<backup name>" \
     --dbpath=/data/duplicati/<backup name>.sqlite \
     --encryption-module=aes \
     --compression-module=zip \
     --dblock-size=500MB \
     --keep-time=7Y \
     --passphrase="password" \
     --disable-module=console-password-input
```


## Run the webui
This is the command I use to run Duplicati as a service.

Unfortunately, there seems to be no easy way to automount a USB drive. So I still have to add a volume for my external USB drive when I am using it. If you have a better solution, please let me know.

```bash
$ docker run \
     --detach \
     --restart=always \
     --name duplicati-in-docker-webui \
     --hostname duplicati-in-docker.local \
     --publish 127.0.0.1:8211:8200 \
     --cpus=2 \
     --memory 4096m \
     --volume "$PWD"/duplicati_storage:/data/duplicati \
     --volume /home/<username>:/home/<username>:ro \
     --volume /opt:/opt:ro \
     registry.gitlab.com/jerrac/duplicati-in-docker:prod \
     duplicati-server --webservice-interface="any" --server-datafolder="/data/duplicati"
```

## Troubleshooting
* Docker run [documentation](https://docs.docker.com/engine/reference/commandline/run/)
* Duplicati [manual](https://duplicati.readthedocs.io/en/latest/)
* Duplicati [forum](https://forum.duplicati.com/)
* [Duplicati source](https://github.com/duplicati/duplicati)
